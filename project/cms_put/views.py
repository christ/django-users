from django.shortcuts import render
from .models import Contenido
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout
from django.shortcuts import redirect, render


def loggedIn(request):
    if request.user.is_authenticated:
        response = "You are logged in as " + request.user.username
    else:
        response = "You are not logged in. <a href='/login'>Login...</a>"
    return HttpResponse(response)


def logout_view(request):
    logout(request)
    return redirect("/cms/")


def index(request):
    contenido = Contenido.objects.all()
    if contenido:
        response = ""
        for content in contenido:
            element = "<br>La clave <a href=" + content.clave + ">" + content.clave + "</a> contiene el valor -> " + content.valor + "</br>"
            response = response + element
    else:
        response = "<p>La lista está vacía</p>"

    response = "You are in the main page. Write /(key) or login with /loggedIn" + response
    return HttpResponse(response)


@csrf_exempt
def get_content(request, llave):
    # Si es PUT
    if request.method == "PUT":
        if not request.user.is_authenticated:
            return HttpResponse(
                "You can't add content with PUT if you are not logged in. <a href='/login'> Log in please")
        else:
            valor = request.body.decode('utf-8')
            try:
                c = Contenido.objects.get(clave=llave)
                c.valor = valor
            except Contenido.DoesNotExist:
                c = Contenido(clave=llave, valor=valor)

            c.save()
    # Si es POST
    if request.method == "POST":
        if not request.user.is_authenticated:
            return HttpResponse(
                "You can't add content with POST if you are not logged in. <a href='/login'> Log in please")
        else:
            valor = request.POST['valor']
            try:
                c = Contenido.objects.get(clave=llave)
                c.valor = valor
            except Contenido.DoesNotExist:
                c = Contenido(clave=llave, valor=valor)

            c.save()
    # GET

    try:
        contenido = Contenido.objects.get(clave=llave)
        response = "El valor -> " + contenido.valor + " pertenece a la clave -> " + contenido.clave
    except Contenido.DoesNotExist:
        if request.user.is_authenticated:
            return render(request, 'cms/form.html', {})
        else:
            response = "You are not logged in. <a href='/login'> Login please"
    return HttpResponse(response)
